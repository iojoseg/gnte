<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Teacher;
use App\Models\City;
use App\Models\Level;
use App\Models\Key;
use App\Models\Type;

class LiveTeacherCreate extends Component
{
	public $name, $lastname_1, $lastname_2, $email, $phone, $profession, $rfc, $sex, $state, $city, $address, $suburb, $street_1, $street_2, $registration_date;
    public $code, $system, $level, $type, $cct, $ct_name, $teacher_id;
    public $inputs = [];
    public $i = 1;

    public function render()
    {
        $cities = City::all();
        $levels = Level::all();
        $types = Type::all();
        return view('livewire.live-teacher-create',["cities" => $cities, "levels" => $levels, "types" => $types]);
    }

     public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function store()
    {
        $validatedDate = $this->validate([
                'registration_date' => 'required',
                'name' => 'required',
                'lastname_1' => 'required',
                'lastname_2' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'profession' => 'required',
                'rfc' => 'required',
                'address' => 'required',
                'suburb' => 'required',
                'street_1' => 'required',
                'street_2' => 'required',
                'code.0' => 'required|max:20|min:20',
                'system.0' => 'required',
                'level.0' => 'required',
                'type.0' => 'required',
                'cct.0' => 'required|max:10|min:10', 
                'ct_name.0' => 'required',
                'code.*' => 'required',
                'system.*' => 'required',
                'level.*' => 'required',
                'type.*' => 'required',
                'cct.*' => 'required|max:10|min:10', 
                'ct_name.*' => 'required',
            ],
            [   
                'registration_date' => 'La fecha de registro es requerida',
                'name.required' => 'El nombre del docente es requerido',
                'lastname_1.required' => 'El apellido paterno es requerido',
                'lastname_2.required' => 'El apellido materno es requerido',
                'email.required' => 'El campo email es requerida',
                'phone.required' => 'El campo telefono es requerido',
                'profession.required' => 'El campo profesion es requerido',
                'rfc.required' => 'El campo RFC es requerido',
                'address.required' => 'El campo dirección es requerido',
                'suburb.required' => 'El campo colonia es requerido',
                'street_1.required' => 'El campo calle es requerida',
                'street_2.required' => 'El campo calle es requerida',
                'code.0.required' => 'La clave presupuestal es requerida',
                'system.0.required' => 'El campo nombre del sistema es requerido',
                'level.0.required' => 'El campo nivel es requerido',
                'type.0.required' => 'El campo tipo es requerido',
                'cct.0.required' => 'El campo cct es requerido', 
                'ct_name.0.required' => 'el campo nombre cct es requerido',
                'code.*.required' => 'La clave presupuestal es requerida',
                'system.*.required' => 'El campo nombre del sistema es requerido',
                'level.*.required' => 'El campo nivel es requerido',
                'type.*.required' => 'El campo tipo es requerido',
                'cct.*.required' => 'El campo cct es requerido', 
                'ct_name.*.required' => 'El campo cct es requerido',
            ]
        );
    	
        $teacher = new Teacher();
        $teacher->registration_date = $this->registration_date;
        $teacher->name = $this->name; 
        $teacher->lastname_1 = $this->lastname_1; 
        $teacher->lastname_2 = $this->lastname_2; 
        $teacher->email = $this->email; 
        $teacher->phone = $this->phone; 
        $teacher->profession = $this->profession; 
        $teacher->rfc = $this->rfc; 
        $teacher->sex = $this->sex; 
        $teacher->state = $this->state; 
        $teacher->city = $this->city; 
        $teacher->address = $this->address; 
        $teacher->suburb = $this->suburb; 
        $teacher->street_1 = $this->street_1; 
        $teacher->street_2 = $this->street_2;
        $teacher->save();


        foreach ($this->code as $key => $value) {
            $budgetary = new Key();
            $budgetary->code = $this->code[$key]; 
            $budgetary->system = $this->system[$key];
            $budgetary->level = $this->level[$key]; 
            $budgetary->type = $this->type[$key]; 
            $budgetary->cct = $this->cct[$key];
            $budgetary->ct_name = $this->ct_name[$key];  
            $budgetary->teacher_id = $teacher->id;
            $budgetary->save();
        }

        return redirect()->to('/dashboard');
    }
}
