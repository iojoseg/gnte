<?php

namespace App\Http\Livewire;

use Livewire\Component;

class LiveTeacherModal extends Component
{
    public function render()
    {
        return view('livewire.live-teacher-modal');
    }
}
