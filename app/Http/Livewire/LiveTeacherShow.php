<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Teacher;
use Illuminate\Support\Facades\DB;

class LiveTeacherShow extends Component
{
	public $IdTeacher;

    public function render()
    {
    	$field = Teacher::find($this->IdTeacher);

    	$claves = DB::table('teachers')
            ->join('keys', 'teachers.id', '=', 'keys.teacher_id')
            ->where('keys.teacher_id','=', $this->IdTeacher)
            ->select('teachers.*', 'keys.*')
            ->get();

        return view('livewire.live-teacher-show',[
            'field' => $field, 'claves' => $claves
        ]);
    }
}
