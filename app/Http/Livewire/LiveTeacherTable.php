<?php

namespace App\Http\Livewire;

use App\Models\Teacher;
use Livewire\Component;
use Livewire\WithPagination;

class LiveTeacherTable extends Component
{
	use WithPagination;

	public $search = '';
	public $perPage = 5;
	

    public function render()
    {
        return view('livewire.live-teacher-table', [
        	'teachers' => Teacher::where('name', 'like', "%{$this->search}%")
        	->orWhere('email', 'like', "%{$this->search}%")
        	->paginate($this->perPage),
        ]);
    }

    public function updatingSearch()
    {
    	$this->resetPage();
    }

  

    public function delete($id)
    {
        Teacher::find($id)->delete();
    }

    
}
