<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Teacher;

class LiveTeacherUpdate extends Component
{
	public $name, $lastname_1, $lastname_2, $email, $phone, $profession, $rfc, $sex, $state, $city, $address, $suburb, $street_1, $street_2, $registration_date;

	public $IdTeacher;

    public function render()
    {
        return view('livewire.live-teacher-update');
    }

    public function edit($id)
    {
    	$teacher = Teacher::findOrFail($id);

        $this->registration_date = $teacher->registration_date;
    	$this->name = $teacher->name;
        $this->lastname_1 = $teacher->lastname_1;
        $this->lastname_2 = $teacher->lastname_2; 
        $this->email = $teacher->email; 
        $this->phone = $teacher->phone; 
        $this->profession = $teacher->profession; 
        $this->rfc = $teacher->rfc; 
        $this->sex = $teacher->sex;
        $this->state = $teacher->state;
        $this->city = $teacher->city; 
        $this->address = $teacher->address; 
        $this->suburb = $teacher->suburb; 
        $this->street_1 = $teacher->street_1;
        $this->street_2 = $teacher->street_2;
    }

    public function update($id)
    {
    	$teacher = Teacher::findOrFail($id);

    	$teacher->update([
                'registration_date' =>$this->registration_date,
                'name' => $this->name, 
            	'lastname_1' => $this->lastname_1, 
            	'lastname_2' => $this->lastname_2, 
            	'email' => $this->email, 
            	'phone' => $this->phone, 
            	'profession' => $this->profession, 
            	'rfc' => $this->rfc, 
            	'sex' => $this->sex, 
            	'state' => $this->state, 
            	'city' => $this->city, 
            	'address' => $this->address, 
            	'suburb' => $this->suburb, 
            	'street_1' => $this->street_1, 
            	'street_2' => $this->street_2
            ]);

    	return redirect()->to('/dashboard');
    }

    public function mount($IdTeacher)
    {
        $this->edit($IdTeacher);
    }
}
