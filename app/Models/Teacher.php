<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'lastname_1', 'lastname_2', 'email', 'phone', 'profession', 'rfc', 'sex','state', 'city','address','suburb', 'street_1', 'street_2','registration_date'
    ];

    protected $dateFormat = 'd/m/Y';

}
