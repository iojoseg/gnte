<?php

namespace Database\Factories;

use App\Models\Teacher;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeacherFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Teacher::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        

        return [
            'name' => $this->faker->name,
            'lastname_1' => $this->faker->lastName,
            'lastname_2' => $this->faker->lastName,
            'profession' => $this->faker->jobTitle,
            'rfc' => Str::random(10),
            'sex' => $this->faker->randomElement(['male', 'female']),
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'address' => $this->faker->address,
            'street_1' => $this->faker->streetAddress,
            'street_2' => $this->faker->streetAddress,
            'suburb' => $this->faker->streetName ,
            'city' => $this->faker->city,
            'state' => $this->faker->state
        ];
    }
}
