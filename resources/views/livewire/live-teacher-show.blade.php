<div>
	<header class="bg-white shadow">
		<div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
			<div class="flex items-center">
				<h2 class="font-semibold text-xl text-gray-800 leading-tight mr-2">
				Información del Docente
				</h2>
				
			</div>
		</div>
	</header>
	<div class="py-12">	
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
				
				<div class="space-y-8 divide-y divide-gray-200 px-12">
					<div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
						<div>
							<div class="pt-8 space-y-6 sm:pt-10 sm:space-y-5">
								<div>
									<h3 class="text-lg leading-6 font-medium text-gray-900">
									Información Personal
									</h3>
									<p class="mt-1 max-w-2xl text-sm text-gray-500">
										Introduza los datos ordenadamente.
									</p>
								</div>
								<div class="space-y-6 sm:space-y-5">
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="email" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Fecha de registro
										</label>
									{{ Carbon\Carbon::parse($field->registration_date)->format('d-m-Y') }}
									</div>

									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="name" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Nombre Completo
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->name }} {{ $field->lastname_1 }} {{ $field->lastname_2}} 
										</div>
									</div>
									
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="email" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Dirección de correo electronico
										</label>
										{{ $field->email }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="phone" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Telefono
										</label>
										{{ $field->phone }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="profession" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Profesión
										</label>
										{{ $field->profession }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="rfc" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											RFC
										</label>
										{{ $field->rfc }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="sex" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Genero
										</label>
										{{ $field->sex }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="state" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Estado
										</label>
										{{ $field->state }}
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="city" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Ciudad
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->city }}
										</div>
									</div>
									<div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
										<label for="address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Dirección
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->address }}
										</div>
										<label for="suburb" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Colonia
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->suburb }}
										</div>
										<label for="street_address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											Entre calles
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->street_1 }}
										</div>
										<label for="street_address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
											
										</label>
										<div class="mt-1 sm:mt-0 sm:col-span-2">
											{{ $field->street_2 }}
										</div>
									</div>		
								</div>
							</div>
							
						</div>
						<div class="pt-5 pb-5">
							
						</div>
					</div>
				</div>


			</div>
			<!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-white shadow overflow-hidden sm:rounded-lg mt-12 shadow-xl ">
  <div class="px-4 py-5 sm:px-6 flex justify-between items-center">
    <div>
    	<h3 class="text-lg leading-6 font-medium text-gray-900">
      		Claves Presupuestales
    	</h3>
    	
    </div>
    <div>
 
    </div>
  </div>
  <div class="border-t border-gray-200 px-4 py-5 sm:px-6">
    <dl class="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
    @foreach($claves as $clave)
      <div class="sm:col-span-2">
       	<div class="grid grid-cols-1 md:grid-cols-3 gap-4 mb-8">
			<div>
  				<label class="block text-sm font-medium text-gray-700">Clave Presupuestal</label>
  				<div class="mt-1">
    				{{ $clave->code }}
  				</div>
			</div>
			<div >
  				<label class="block text-sm font-medium text-gray-700">Sistema</label>
  				<div class="mt-1">
    				{{ $clave->system }}
  				</div>
			</div>
			<div >
  				<label class="block text-sm font-medium text-gray-700">Nivel</label>
  				<div class="mt-1">
    				{{ $clave->level }}
  				</div>
			</div>
			<div >
  				<label class="block text-sm font-medium text-gray-700">Tipo</label>
  				<div class="mt-1">
    				{{ $clave->type }}
  				</div>
			</div>
			<div >
  				<label class="block text-sm font-medium text-gray-700">C.C.T</label>
  				<div class="mt-1">
    				{{ $clave->cct }}
  				</div>
			</div>
			<div >
  				<label class="block text-sm font-medium text-gray-700">Nombre C.T</label>
  				<div class="mt-1">
    				{{ $clave->ct_name }}
  				</div>
			</div>
       	</div>

      </div>
      @endforeach
    </dl>
    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">							
	</div>
  </div>
</div>

		</div>
	</div>
	
</div>