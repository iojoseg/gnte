<div>
    <header class="bg-white shadow">
    	<div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        	<div class="flex items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight mr-2">
                Editar el registro del Docente {{ $IdTeacher }}
            </h2>
        		
        	</div>
        </div>
    </header>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
  
<form class="space-y-8 divide-y divide-gray-200 px-12">
  <div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
    <div>
    <div class="pt-8 space-y-6 sm:pt-10 sm:space-y-5">
      <div>
        <h3 class="text-lg leading-6 font-medium text-gray-900">
          Información Personal
        </h3>
        <p class="mt-1 max-w-2xl text-sm text-gray-500">
          Introduza los datos ordenadamente.
        </p>
      </div>
      <div class="space-y-6 sm:space-y-5">
        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="name" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
                      Fecha de registro
          </label>
              <div class="mt-1 sm:mt-0 sm:col-span-2">
                 <input type="date" wire:model="registration_date" id="date" autocomplete="given-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
              </div>
          </div>
        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="name" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Nombre
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="name" id="name" autocomplete="given-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="lastname_1" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Apellido paterno
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="lastname_1" id="lastname_1" autocomplete="family-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="lastname_2" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Apellido materno
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="lastname_2" id="lastname_2" autocomplete="family-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="email" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Dirección de correo electronico
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input id="email" wire:model="email" type="email" autocomplete="email" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="phone" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Telefono
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="number" wire:model="phone" id="phone" autocomplete="family-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="profession" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Profesión
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input id="profession" wire:model="profession" type="text" autocomplete="email" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="rfc" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            RFC
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="rfc" id="rfc" autocomplete="family-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="sex" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Genero
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <select id="sex" wire:model="sex" autocomplete="country" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
              <option>Femenino</option>
              <option>Masculino</option>
            </select>
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="state" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Estado
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <select id="state" wire:model="state" autocomplete="country" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
              <option>United States</option>
              <option>Canada</option>
              <option>Mexico</option>
            </select>
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="city" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Ciudad
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="city" id="city" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
        </div>

        <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
          <label for="address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Dirección
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="address" id="address" autocomplete="street-address" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
          </div>

          <label for="suburb" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Colonia
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="suburb" id="suburb" autocomplete="family-name" class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md">
          </div>
          <label for="street_address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            Entre calles
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="street_1" id="street_1" autocomplete="street-address" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
          </div>
          <label for="street_address" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">
            
          </label>
          <div class="mt-1 sm:mt-0 sm:col-span-2">
            <input type="text" wire:model="street_2" id="street_2" autocomplete="street-address" class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md">
          </div>

        </div>
 
      </div>
    </div>

 
  </div>

  <div class="pt-5 pb-5">
    <div class="flex justify-end">
      <button wire:click="update({{ $IdTeacher }})" class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        Guardar
      </button>
    </div>
  </div>
</form>

            </div>
        </div>

        <div class="my-8">
          <a href="{{ route('dashboard') }}" class="inline-flex items-center px-6 py-3 border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Regresar
            
            <svg class="ml-3 -mr-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 15v-1a4 4 0 00-4-4H8m0 0l3 3m-3-3l3-3m9 14V5a2 2 0 00-2-2H6a2 2 0 00-2 2v16l4-2 4 2 4-2 4 2z" />
            </svg>
          </a>
        </div>
    </div>
</div>
